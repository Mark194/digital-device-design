module v53_4sum(a,b,s,p);
input [3:0] a,b;
output [3:0] s;
output p;
assign {p,s}=a+b;
endmodule 