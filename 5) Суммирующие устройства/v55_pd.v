module v55_pd(d,q,p);
input [3:0]d;
output [3:0]q;
output [2:0]p;
assign q[3]=d[3];
assign p[2]=d[3]&(d[3]^d[2]);
assign q[2]=d[3]^(d[3]^d[2]);
assign p[1]=d[3]&(d[3]^d[1]);
assign q[1]=d[3]^(d[3]^d[1]);
assign p[0]=d[3]&(d[3]^d[0]);
assign q[0]=d[3]^(d[3]^d[0]);
 endmodule 