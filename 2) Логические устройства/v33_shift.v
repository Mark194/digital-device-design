module v33_shift(d,q,c);
input [3:0]d;
output [4:0]q;
output c;
assign {q}={d};
endmodule 