module v22_shift(d,q,c);
input [3:0]d;
output [3:0]q;
output c;
assign {q,c}={d};
endmodule 