module v21_ks(a,b,vand,vor,vxor);
input [3:0] a,b;
output [3:0] vand,vor,vxor;
assign vand=a&b;
assign vor=a|b;
assign vxor=a^b;
endmodule 