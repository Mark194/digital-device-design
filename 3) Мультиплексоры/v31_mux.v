module v31_mux(x,y,a,q);
input x,y,a;
output q;
assign q = a? x:y;
endmodule 