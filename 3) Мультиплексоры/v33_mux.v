module v33_mux(x,a,q);
input [7:0]x;
input [2:0]a;
output reg q;
always @*
case ({a})
3'b000: q=x[0];
3'b001: q=x[1];
3'b010: q=x[2];
3'b011: q=x[3];
3'b100: q=x[4];
3'b101: q=x[5];
3'b110: q=x[6];
3'b111: q=x[7];
endcase
endmodule 