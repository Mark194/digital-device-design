module v35_mux(d,q); 
input [4:0]d; 
output reg q; 
always @* 
case ({d}) 
5'b00110: q=1; 
5'b01010: q=1; 
5'b01111: q=1; 
5'b11010: q=1; 
default: q=0; 
endcase 
endmodule 