module v34_mux(x,a,q);
input [7:0]x;
input [2:0]a;
output reg q;
always @*
case ({a})
3'b001: q=x[0];
3'b010: q=x[3];
3'b101: q=x[5];
3'b111: q=x[6];
default: q=0;
endcase
endmodule 