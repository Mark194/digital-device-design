module v41_dc(d,q);
input [1:0]d;
output [3:0]q;
assign q[0]=~d[1]&~d[0];
assign q[1]=~d[1]&d[0];
assign q[2]=d[1]&~d[0];
assign q[3]=d[1]&d[0];
endmodule 