module v45_gb(g,b);
input [3:0]g;
output [3:0]b;
assign b[3]=g[3];
assign b[2]=^g[3:2];
assign b[1]=^g[3:1];
assign b[0]=^g[3:0];
endmodule
