module v46_led(d,i);
input [3:0]d;
output reg [6:0]i;
always @*
case(d)
4'b0000: i=7'b0000001;
4'b0001: i=7'b1001111;
4'b0010: i=7'b0010010;
4'b0011: i=7'b0000110;
4'b0100: i=7'b1001100;
4'b0101: i=7'b0100100;
4'b0110: i=7'b0100000;
4'b0111: i=7'b0001111;
4'b1000: i=7'b0000000;
4'b1001: i=7'b0000100;
default: i=7'b1111111;
endcase
endmodule 