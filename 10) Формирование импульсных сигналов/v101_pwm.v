module v101_pwm (c, q, d, g);
input c;
input [3:0] d;
output [3:0] q;
reg [3:0] q;
output g;
always @(posedge c) q = q +1;
assign g=d>q;
endmodule
