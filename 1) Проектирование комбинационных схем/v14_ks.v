module v14_ks(d,q);
input [2:0]d;
output q;
assign q=d[0]|(d[1]&(d[2]^d[1]));
endmodule 