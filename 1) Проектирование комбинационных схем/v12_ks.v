module v12_ks (d,x,y,q);
input [2..0] d;
output x,y,q;
assign x=d[2]&d[1]&d[0];
assign y=d[2]|d[1]|d[0];
assign q=d[2]^x^y;
endmodule;
