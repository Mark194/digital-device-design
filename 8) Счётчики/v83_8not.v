module v83_8not(d,q);
input [7:0]d;
output [7:0]q;
assign q=~d;
endmodule 