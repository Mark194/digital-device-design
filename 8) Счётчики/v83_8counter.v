module v83_8counter(c,q);
input c;
output [7:0]q;
reg [7:0]q;
always @ (posedge c)
q=q+1;
endmodule 