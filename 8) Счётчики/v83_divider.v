module v83_divider(clk,x,w);
input clk;
output x;
output w;
reg [22:0]w;
always @(posedge clk)
w=w+1;
assign x=w[2];
endmodule
