module v72_d_ds(c,d,q_stat,q_din);
input c,d;
output q_stat, q_din;
reg q_din;
assign q_stat = c ? d : q_stat;
always @ (posedge c) q_din=d;
endmodule 